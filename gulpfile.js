var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
// var uglify = require('gulp-uglify');
// var imagemin = require('gulp-imagemin');

gulp.task('browserSync', function(){
  
  browserSync.init({
    server: "./"
  });
	gulp.watch('sass/**/*.scss', gulp.series('sass'));
	// gulp.watch('raw-assets/sass/*-min.scss', gulp.series('sass-min'));
  gulp.watch('*.html', gulp.series('html'));
  // gulp.watch('raw-assets/js/lib/*-min.js', gulp.series('scripts'));
  gulp.watch('assets/js/main.js', gulp.series('main_script'));
  // gulp.watch('raw-assets/img/**/*', gulp.series('imagemin'));
});

gulp.task('sass', function () {
  return gulp.src('sass/**/*.scss')
    .pipe(sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError))
    .pipe(autoprefixer(['last 99 versions'], { cascade: true }))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
});

// gulp.task('sass-min', function () {
// return gulp.src('raw-assets/sass/*-min.scss')
// 	.pipe(sass({
// 		outputStyle: 'compressed'
// 	}).on('error', sass.logError))
// 	.pipe(autoprefixer(['last 99 versions'], { cascade: true }))
// 	.pipe(gulp.dest('assets/css/lib'))
// 	.pipe(browserSync.stream());
// });

gulp.task('html', function () {
  return gulp.src('*.html')
    .pipe(browserSync.stream());
});


gulp.task('main_script', function () {
  return gulp.src('assets/js/main.js')
    .pipe(browserSync.stream());
});

// gulp.task('scripts', function () {
//   return gulp.src('raw-assets/js/lib/fis-animate-min.js')
//     .pipe(browserSync.stream());
// });

// gulp.task('scripts', function() {
//   return gulp.src('raw-assets/js/lib/*-min.js')
//     .pipe(
// 		uglify({
// 			options: {
// 			  preserveComments: /(?:^!|@(?:license|preserve|cc_on))/
// 			}
// 		})
// 	)
//     .pipe(gulp.dest('assets/js/lib/'))
// 	.pipe(browserSync.stream());
// });

// gulp.task('imagemin', function() {
//     return gulp.src('raw-assets/img/**/*')
//            .pipe(imagemin({
//                 progressive: true
//            }))
//            .pipe(gulp.dest('assets/img/'))
// });

gulp.task('default', gulp.series('browserSync'));